from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=20, unique=True)
    import_href = models.CharField(max_length=200, unique=True)
    sold = models.BooleanField(default=False, null=True)

    def __str__(self):
        return f"{self.import_href}"


class Salesperson(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.CharField(max_length=200, null=True)

    def __str__(self):
        return f"{self.name}"


class PotentialCustomer(models.Model):
    name = models.CharField(max_length=200)
    street = models.CharField(max_length=200)
    city = models.CharField(max_length=200)
    state = models.CharField(max_length=200)
    zip_code = models.CharField(max_length=200)
    phone = models.CharField(max_length=200)

    def __str__(self):
        return f"{self.name}"


class Sales(models.Model):
    sale_price = models.PositiveBigIntegerField()

    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sales",
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey(
        PotentialCustomer,
        related_name="sales",
        on_delete=models.CASCADE,
    )
    salesperson = models.ForeignKey(
        Salesperson,
        related_name="sales",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_show_sales", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.automobile} {self.customer} {self.salesperson}"
