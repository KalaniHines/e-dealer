from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Salesperson, Sales, AutomobileVO, PotentialCustomer
import json


class SalesPersonListEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        'name',
        'employee_number',
        'id',
    ]


class AutoMobileVOListEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        'vin',
        'import_href',
        'sold',
        'id',
    ]

class PotentialCustomerListEncoder(ModelEncoder):
    model = PotentialCustomer
    properties = [
        'name',
        'id',
        'street',
        'city',
        'zip_code',
        'state',
        'phone',
    ]

class SalesListEncoder(ModelEncoder):
    model = Sales
    properties = [
        'sale_price',
        'automobile',
        'customer',
        'salesperson',
        'id',
    ]
    encoders = {
        'salesperson': SalesPersonListEncoder(),
        'automobile': AutoMobileVOListEncoder(),
        'customer': PotentialCustomerListEncoder(),
    }


@require_http_methods(['GET', 'POST'])
def api_list_salesperson(request):
    if request.method == 'GET':
        salesperson = Salesperson.objects.all()
        return JsonResponse(
            {'salesperson': salesperson},
            encoder=SalesPersonListEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalesPersonListEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"Error": "Can not create salesperson"},
            )
            response.status_code = 400
            return response

@require_http_methods(['GET'])
def api_show_salesperson(request, pk):
    if request.method == 'GET':
        salesperson = Salesperson.objects.get(id=pk)
        return JsonResponse(
            salesperson,
            encoder=SalesPersonListEncoder,
            safe=False,
        )

@require_http_methods(['GET', 'POST'])
def api_list_customer(request):
    if request.method == 'GET':
        customer = PotentialCustomer.objects.all()
        return JsonResponse(
            {'customer': customer},
            encoder=PotentialCustomerListEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            customer = PotentialCustomer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=PotentialCustomerListEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"Error": "Can not create customer"},
            )
            response.status_code = 400
            return response

@require_http_methods(['GET', 'POST'])
def api_list_sales(request):
    if request.method == 'GET':
        sales = Sales.objects.all()
        return JsonResponse(
            {'sales': sales},
            encoder=SalesListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            autos = AutomobileVO.objects.get(import_href=content['import_href'])
            if autos.sold==False:
                autos.sold=True
                autos.save()
                autos = AutomobileVO.objects.get(import_href=content['import_href'])
                content['automobile'] = autos

                customer = PotentialCustomer.objects.get(name=content['customer'])
                content['customer'] = customer

                salesperson = Salesperson.objects.get(name=content['salesperson'])
                content['salesperson'] = salesperson
                sales = Sales.objects.create(
                    #**content
                    sale_price=content['sale_price'],
                    automobile=autos,
                    customer=content['customer'],
                    salesperson=content['salesperson']
                    )
                return JsonResponse(
                    sales,
                    encoder=SalesListEncoder,
                    safe=False,
                )
            else:
                response = JsonResponse(
                    {"Error": "Automobile already sold"},
                )
            response.status_code = 400
            return response
        except Exception as e:
            return JsonResponse(
        {"Error": "Can not create sale", "Exception": str(e)},
        )


@require_http_methods(['GET'])
def api_show_sales(request, pk):
    if request.method == 'GET':
        sales = Sales.objects.get(id=pk)
        return JsonResponse(
            sales,
            encoder=SalesListEncoder,
            safe=False,
        )

@require_http_methods(['GET'])
def api_list_automobilevo(request):
    if request.method == 'GET':
        autos = AutomobileVO.objects.all()
        return JsonResponse(
            {'autos': autos},
            encoder=AutoMobileVOListEncoder,
        )
