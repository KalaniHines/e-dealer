from django.urls import path
from .api_views import (
    api_list_salesperson,
    api_list_customer,
    api_list_automobilevo,
    api_list_sales,
    api_show_salesperson,
    api_show_sales,
)


urlpatterns = [
    path('salesperson/', api_list_salesperson, name='api_create_salesperson'),
    path('customer/', api_list_customer, name='api_create_customer'),
    path('automobilevo/', api_list_automobilevo, name='automobile_list'),
    path('sales/', api_list_sales, name='api_create_sales'),

    path('sales/<int:pk>/', api_show_sales, name='api_sales_details'),
    path('salesperson/<int:pk>/', api_show_salesperson, name='api_show_salesperson'),
]
