import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

from sales_rest.models import AutomobileVO

def get_automobiles():
    response = requests.get('http://inventory-api:8000/api/automobiles/')
    content = json.loads(response.content)
    #print("data rec.", content)
    for automobile in content['autos']:
        #print("poller content", content['autos'])

        AutomobileVO.objects.update_or_create(
            import_href=automobile['href'],
            defaults={
            'vin': automobile['vin'],
            },
        )
        #print("auto update: Jon is rad")

def poll():
    while True:
        print('Sales poller polling for data')
        try:
            get_automobiles()

        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(30)


if __name__ == "__main__":
    poll()


# {'autos': [{'href': '/api/automobiles/1235465799900/', 'id': 1, 'color': 'Blue', 'year': 2016,
#  'vin': '1235465799900', 'model': {'href': '/api/models/1/', 'id': 1, 'name': 'LX', 'picture_url': '',
#  'manufacturer': {'href': '/api/manufacturers/1/', 'id': 1, 'name': 'Chevy'}}},

#  {'href': '/api/automobiles/123451234512345/', 'id': 2, 'color': 'red', 'year': 2020,
# 'vin': '123451234512345', 'model': {'href': '/api/models/2/', 'id': 2, 'name': 'Miata',
#  'picture_url': 'https://hips.hearstapps.com/hmg-prod/images/2023-maza-mx-5-miata-102-1669758443.jpg',
#    'manufacturer': {'href': '/api/manufacturers/2/', 'id': 2, 'name': 'Mazda'}}}]}
