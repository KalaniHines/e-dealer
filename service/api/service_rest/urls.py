from django.urls import path

from .views import api_technician, api_service_appointments, api_show_appointments


urlpatterns = [
    path("technicians/", api_technician, name="api_technician"),
    path("appointments/", api_service_appointments , name="api_service_appointments"),
    path("appointments/<int:pk>/", api_show_appointments, name="api_show_appointments"),
]
