from django.db import models
from django.urls import reverse
# Create your models here.


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)

    def get_api_url(self):
        return reverse("api_automobile", kwargs={"vin": self.vin})

class Tech(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.PositiveSmallIntegerField(unique=True)

    def get_api_url(self):
        return reverse("api_technician", kwargs={"pk": self.id})

class ServiceAppointment(models.Model):
    date = models.DateField()
    time = models.TimeField()
    customer_name = models.CharField(max_length=200)
    reason = models.CharField(max_length=200)
    completed = models.BooleanField(default=False)
    vip = models.BooleanField(default=False)
    technician = models.ForeignKey(
        Tech,
        related_name="appointments",
        on_delete=models.CASCADE,
    )
    vin = models.CharField(max_length=17)

    def get_api_url(self):
        return reverse("api_show_appointments", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.vin} {self.time}"


    @classmethod
    def create(cls, **kwargs):
        kwargs["vip"] = AutomobileVO.objects.filter(
            vin=kwargs["vin"]).count() > 0
        appointment = cls(**kwargs)
        appointment.save()
        return appointment
