import React, {useState} from "react";

function NewCustomerForm() {
    const [name, setName] = useState("");
    const [street, setStreet] = useState("");
    const [city, setCity] = useState("");
    const [state, setState] = useState("");
    const [zip, setZip] = useState("");
    const [phone, setPhone] = useState("");

    const handleNameChange = (e) => {
        const value = e.target.value;
        setName(value);
    }
    const handleStreetChange = (e) => {
        const value = e.target.value;
        setStreet(value);
    }
    const handleCityChange = (e) => {
        const value = e.target.value;
        setCity(value);
    }
    const handleStateChange = (e) => {
        const value = e.target.value;
        setState(value);
    }
    const handleZipChange = (e) => {
        const value = e.target.value;
        setZip(value);
    }
    const handlePhoneChange = (e) => {
        const value = e.target.value;
        setPhone(value);
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {};
        data.name = name;
        data.street = street;
        data.city = city;
        data.state = state;
        data.zip_code = zip;
        data.phone = phone;


        const createCustomerUrl = 'http://localhost:8090/api/customer/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(createCustomerUrl, fetchConfig);
        if (response.ok) {
            const newCustomer = await response.json();
            setName("");
            setStreet("");
            setCity("");
            setState("");
            setZip("");
            setPhone("");


        }
    }

    return (
        <div className="my-5 container">
        <div className="row">
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
                <form onSubmit={handleSubmit} id="customer-form">
                <h1 className="card-title">New Customer</h1>
                <p className="mb-3">
                Please fill out this form to create a new customer.
                </p>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleNameChange} required placeholder="name" type="text" id="name" name="name" className="form-control" value={name}/>
                      <label htmlFor="name">Customer Name</label>
                  </div>
                  <div className="col">
                  <div className="form-floating mb-3">
                    <input onChange={handleStreetChange} required placeholder="street" type="text" id="street" name="street" className="form-control" value={street}/>
                    <label htmlFor="street">Street</label>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                        <input onChange={handleCityChange} required placeholder="city" type="text" id="city" name="city" className="form-control" value={city}/>
                        <label htmlFor="city">City</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                        <input onChange={handleStateChange} required placeholder="state" type="text" id="state" name="state" className="form-control" value={state}/>
                        <label htmlFor="state">State</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                        <input onChange={handleZipChange} required placeholder="zip" type="text" id="zip" name="zip" className="form-control" value={zip}/>
                        <label htmlFor="zip">Zip</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                        <input onChange={handlePhoneChange} required placeholder="phone" type="text" id="phone" name="phone" className="form-control" value={phone}/>
                        <label htmlFor="phone">Phone</label>
                    </div>
                  </div>
                  <div className="col">
                    <button type="submit" className="btn btn-primary">Submit</button>
                  </div>
            </div>
            </div>
            </form>
            </div>
          </div>
        </div>
        </div>
        </div>
    );


}

export default NewCustomerForm;
