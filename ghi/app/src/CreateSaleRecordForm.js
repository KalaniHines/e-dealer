import React, { useEffect, useState } from 'react';

function CreateSaleRecord() {
    const [autos, setAutos] = useState([]);
    const [salespersons, setSalesperson] = useState([]);
    const [customers, setCustomer] = useState([]);

    const [auto, setAuto] = useState('');
    const [salespersonId, setSalespersonId] = useState('');
    const [customerId, setCustomerId] = useState('');
    const [price, setPrice] = useState('');

    const handleAutosChange = (e) => {
        setAuto(e.target.value);
    }
    const handleSalespersonChange = (e) => {
        setSalespersonId(e.target.value);
    }
    const handleCustomerChange = (e) => {
        setCustomerId(e.target.value);
    }
    const handlePriceChange = (e) => {
        setPrice(e.target.value);
    }


    const handleSubmit = async (e) => {
        e.preventDefault();

        const data = {};
        data.automobile = auto;
        data.salesperson = salespersonId;
        data.customer = customerId;
        data.sale_price = price;

        const saleUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };

        const response = await fetch(saleUrl, fetchConfig);
        if (response.ok) {
            const newSaleRecord = await response.json();
            setAuto('');
            setSalespersonId('');
            setCustomerId('');
            setPrice('');
        }
    }

    const fetchAutos = async () => {
        const autoUrl = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(autoUrl);

        if (response.ok) {
            const data = await response.json();
            setAutos(data.autos);
        }

    }

    const fetchSalesperson = async () => {
        const salespersonUrl = 'http://localhost:8090/api/salesperson/';
        const response = await fetch(salespersonUrl);
        if (response.ok) {
            const salespersonResp = await response.json();
            setSalesperson(salespersonResp.salesperson);
        }
    }
    const fetchCustomer = async () => {
        const customerUrl = 'http://localhost:8090/api/customer/';
        const response = await fetch(customerUrl);
        if (response.ok) {
            const customerRes = await response.json();
            setCustomer(customerRes.customer);
        }
    }

    useEffect(() => {
        fetchAutos();
        fetchSalesperson();
        fetchCustomer();
    }, []);


    return (
        <div className="my-5 container">
        <div className="row">
            <div className="col">
                <div className="card shadow">
                    <div className="card-body">
                    <form onSubmit={handleSubmit} id="sale-form">
                      <h1 className="card-title">Create Sale Record</h1>
                      <div className="col">
                        <div className="form-floating mb-3">
                            <select onChange={handleAutosChange} placeholder="Auto" type="text" id="Auto" name="Auto" className="form-control" value={auto}>

                            <option key="default" value="">Add an automobile</option>
                            {autos.map(a => {
                                return (
                                    <option key={a.vin} value={a.vin}>{a.vin}</option>
                                )

                            })}
                            </select>
                        </div>

                        <div className="form-floating mb-3">
                            <select onChange={handleSalespersonChange} placeholder="Salesperson" type="text" id="Salesperson" name="Salesperson" className="form-control" value={salespersonId}>
                            <option key="default" value="">Add a salesperson</option>
                                {salespersons.map(s => {
                                    return (
                                        <option key={s.id} value={s.name}>{s.name}</option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                           <select onChange={handleCustomerChange} placeholder="Customer" type="text" id="Customer" name="Customer" className="form-control" value={customerId}>

                            <option key="default" value="">Add a customer</option>
                            {customers.map(c => {
                                return (
                                    <option key={c.id} value={c.name}>{c.name}</option>
                                )
                            })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePriceChange} placeholder="Price" type="text" id="Price" name="Price" className="form-control" value={price}  />
                            <label htmlFor="Price">Add a price</label>
                        </div>
                        <div className="col">
                            <button type="submit" className="btn btn-primary">Create</button>
                        </div>
                        </div>

                    </form>
                    </div>
                </div>
            </div>
        </div>
        </div>
    )


}

export default CreateSaleRecord;
