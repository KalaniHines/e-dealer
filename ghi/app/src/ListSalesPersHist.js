import React, {useState, useEffect} from 'react';


function SalesPersHist() {
    const [sales, setSales] = useState([]);
    const [salesPerson, setSalesPerson] = useState([]);
    const [filteredSales, setFilteredSales] = useState([]);
    const [name, setName] = useState('');



    async function loadSalesPerson() {
        const response = await fetch('http://localhost:8090/api/salesperson/')
        if (response.ok) {
            const data = await response.json();
            setSalesPerson(data.salesperson);
        }
    }

    async function loadSales(value) {

        const salesResponse = await fetch('http://localhost:8090/api/sales/')
        if (salesResponse.ok) {
            try {
                const salesData = await salesResponse.json();
                setSales(salesData);
                const saleDetails = salesData.sales;
                const temp = [];
                for (const saleDetail of saleDetails) {
                    if (saleDetail.salesperson.employee_number === value) {
                        temp.push(saleDetail);
                    }
                }
                setFilteredSales(temp);

            } catch (error) {
                console.log("error:", error)
            }
        }
    }


    const handleSalesPersonChange = async (e) => {
        const value = e.target.value;
        loadSales(value);
        setName(value);
    }


    useEffect(() => {
        loadSalesPerson();
    }, []);




    return (
        <>
            <div className="container">
                <div className="col col-sm-auto">
                    <div className="card shadow p-4 mt-4">
                    <div className="card-body">
                        <h1 className="card-title">Sales History</h1>
                        <p className="mb-3">Please select sales person.</p>
                        <div className="form-floating mb-3">
                            <select onChange={handleSalesPersonChange} placeholder="seller" type="text" id="seller" name="seller" className="form-select">
                                <option key="default" value="">Choose an employee</option>
                                {salesPerson.map((item, i) => {
                                    return (
                                        <option key={i} value={item.employee_number}>{item.name}</option>
                                    );
                                })}
                            </select>
                        </div>
                    </div>
                    <table className='table table-striped'>
                        <thead>
                            <tr>
                                <th>Salesperson</th>
                                <th>Customer</th>
                                <th>VIN</th>
                                {/* <th>Veh Name</th> */}
                                <th>Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            {filteredSales.map((item, i) => {
                                return (
                                    <tr key={i}>
                                        <td> {item.salesperson.name}</td>
                                        <td> {item.customer.name}</td>
                                        <td> {item.automobile.vin}</td>
                                        <td> {item.sale_price}</td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
            </div>
        </>
    )
}

export default SalesPersHist;
