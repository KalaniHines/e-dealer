import React, {useState, useEffect} from 'react';


function SalesList() {
    const [sales, setSales] = useState([]);
    const fetchSales = async () => {
        const response = await fetch('http://localhost:8090/api/sales/');
        const data = await response.json();
        setSales(data.sales);
    }
    useEffect(() => {
        fetchSales();
    }, []);

    return (
        <table className="table table-striped">
        <thead>
            <tr>
                <th>Employee Name</th>
                <th>Employee Number</th>
                <th>Purchaser Name</th>
                <th>VIN</th>
                <th>Price</th>
            </tr>
        </thead>
        <tbody>
            {sales?.map(sale => {
                return (
                    <tr key={sale.id}>
                        <td>{sale.salesperson.name}</td>
                        <td>{sale.salesperson.employee_number}</td>
                        <td>{sale.customer.name}</td>
                        <td>{sale.automobile.vin}</td>
                        <td>${sale.sale_price.toLocaleString() }</td>
                    </tr>
                );
            })}
        </tbody>
        </table>
        );
}

export default SalesList;
