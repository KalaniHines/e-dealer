import React, { useEffect, useState } from 'react';

function AutomobileForm( ) {

    const [models, setModels] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);
        if (response.ok){
            const data = await response.json();
            setModels(data.models)
        }
    };

    const[vin, setVIN] = useState('')
    const[color, setColor] = useState('')
    const[year, setYear] = useState('')
    const[model, setModel] = useState('')

        const handleVINChange = (event) => {
            const value = event.target.value;
            setVIN(value);
            }
        const handleColorChange = (event) => {
            const value = event.target.value;
            setColor(value);
            }
        const handleYearChange = (event) =>{
            const value = event.target.value;
            setYear(value)
        }
        const handleModelChange = (event) =>{
            const value = event.target.value;
            setModel(value)
        }

        const handleSubmit = async (event) => {
            event.preventDefault();

            const data = {};

            data.color = color;
            data.year = year;
            data.vin = vin;
            data.model_id = model;

            const AutomobileUrl = 'http://localhost:8100/api/automobiles/';
            const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
            };

        const response = await fetch(AutomobileUrl, fetchConfig);
        if (response.ok) {

            const newAutomobile= await response.json();

            setVIN('');
            setColor('');
            setYear('');
            setModel('');

        }
    }

    useEffect(() => {
        fetchData();
        }, []);

    return (
    <div className="container">
    <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Create a new Automobile</h1>
            <form onSubmit={handleSubmit} id="create-automobile-form">
            <div className="form-floating mb-3">
                <input onChange={handleVINChange} value={vin} placeholder="vin" required type="text" name="vin" id="vin" className="form-control"></input>
                <label htmlFor="vin">VIN</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleColorChange} value={color} placeholder="color" required type="text" name="color" id="color" className="form-control"></input>
                <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleYearChange} value={year} placeholder="year" required type="text" name="year" id="year" className="form-control"></input>
                <label htmlFor="year">Year</label>
            </div>
            <div className="mb-3">
                <select onChange={handleModelChange} value={model} required id="model"  name="model" className="form-select">
                    <option value="" >Choose a Model</option>
                    {models.map(model =>{
                        return (
                            <option value={model.id} key={model.id} >
                            {model.name}
                            </option>
                        );
                    })}
                </select>
            </div>
            <button className="btn btn-primary">Create</button>
            </form>
        </div>
        </div>
    </div>
    </div>
    );
}

export default AutomobileForm;
