import React, { useEffect, useState } from 'react';

function CreateModelForm() {
    const [name, setName] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [manufacturers, setManufacturers] = useState([]);

    const handleNameChange = (e) => {
        setName(e.target.value);
    }
    const handlePictureUrlChange = (e) => {
        setPictureUrl(e.target.value);
    }
    const handleManufacturerChange = (e) => {
        setManufacturer(e.target.value);
    }

    const handleSubmit = async (e) => {
        e.preventDefault();

        const data = {};
        data.name = name;
        data.picture_url = pictureUrl;
        data.manufacturer_id = manufacturer;

        const modUrl = 'http://localhost:8100/api/models/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(modUrl, fetchConfig);
        if (response.ok) {
            const newMod = await response.json();
            setName('');
            setPictureUrl('');
            setManufacturer('');
        }
    }
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="my-5 container">
        <div className="row">
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form onSubmit={handleSubmit} id="hat-form">
                <h1 className="card-title">Create a vehicle model</h1>
                  <div className="col">
                      <div className="form-floating mb-3">
                        <input onChange={handleNameChange} required placeholder="name" type="text" id="name" name="name" className="form-control" value={name}/>
                        <label htmlFor="style_name">Name</label>
                      </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                        <input onChange={handlePictureUrlChange} required placeholder="picture" type="text" id="picture" name="picture" className="form-control" value={pictureUrl}/>
                        <label htmlFor="picture">Picture URL</label>
                    </div>
                  </div>
                  <div className="form-floating mb-3">
                    <select onChange={handleManufacturerChange} name="manufacturer" id="manufacturer" required value={manufacturer}>
                      <option key="default" value="">Choose a manufacturer</option>
                      {manufacturers.map(manufacturer => {
                        return (
                          <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                        )
                      })}
                    </select>
                  </div>

                  <div className="col">
                    <button type="submit" className="btn btn-primary">Create</button>
                  </div>
              </form>
              </div>
              </div>
              </div>
              </div>
              </div>
    )
}


export default CreateModelForm;
