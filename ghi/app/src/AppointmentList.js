import {useState, useEffect} from 'react'


function AppointmentList() {

    const[appointments, setAppointments] = useState([])

    const getAppointments = async () => {
    const appointmentsUrl = 'http://localhost:8080/api/appointments/'
    const appointmentsResponse = await fetch(appointmentsUrl);

    if (appointmentsResponse.ok){
        const data = await appointmentsResponse.json()
        const appointment = data.appointments
        setAppointments(appointment)
        }
    }

    const deleteAppointment = async (id) => {
        fetch(`http://localhost:8080/api/appointments/${id}/`, {
            method: "DELETE"
        });
        getAppointments();
        await getAppointments();
    };

    const handleSubmit = (id) => {
        completeAppointment(id);
        };


    const completeAppointment = async (id) => {
        const appointmentToUpdate = appointments.find(
            (appointment) => appointment.id === id
        );
        appointmentToUpdate.completed = true;
        const fetchConfig = {
            method: "PUT",
            body: JSON.stringify({completed: true}),
            headers: {
                "Content-Type": "application/json",
            },
        }

        await fetch(`http://localhost:8080/api/appointments/${id}/`, fetchConfig);
            getAppointments();
        };



        const filteredAppointments = appointments.filter(
            (appointment) => !appointment.completed
            );

    useEffect(() => {
        getAppointments();
        }, []);
return (
        <>
            <div className="container">
                <table className="table table-striped">
                <thead>
                    <tr>
                    <th>VIN</th>
                    <th>Customer Name</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Technician</th>
                    <th>VIP</th>
                    <th>Reason</th>
                    <th>Completed</th>
                    <th>Cancel </th>
                    <th>Mark As Complete</th>
                    </tr>
                </thead>
                <tbody>
            {filteredAppointments.map((appointment) => (
                <tr key={appointment.id}>
                <td>{appointment.vin}</td>
                <td>{appointment.customer_name}</td>
                <td>{appointment.date}</td>
                <td>{appointment.time}</td>
                <td>{appointment.technician?.name}</td>
                <td>{appointment.vip ? "Yes" : "No"}</td>
                <td>{appointment.reason}</td>
                <td>{appointment.completed ? "Yes" : "No"}</td>
                <td className="align-middle">
                    <button onClick={() => deleteAppointment(appointment.id)} type="button" className="btn btn-danger">Cancel
                    </button>
                    </td>
                    <td className="align-middle">
                    <button
                    onClick={() => handleSubmit(appointment.id)}type="button"className="btn btn-success">Complete
                    </button>
                    </td>
                </tr>
                ))}
            </tbody>
            </table>
        </div>
        </>
    );
    }

    export default AppointmentList;
